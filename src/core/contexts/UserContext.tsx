
import React, { FC, Reducer, useReducer } from 'react'
import { UserProfile } from '../model/User'
import { userService } from '../services'

interface UserContext {
  user: UserProfile | null,
  loggedIn: boolean,
  login: Function
  logout: Function
}


export const UserContext = React.createContext<UserContext>({
  user: null,
  loggedIn: false,
  login: () => { },
  logout: () => { },
})

interface State {
  user: UserProfile | null;
  loggedIn: boolean;
}
const initialState = {
  user: null,
  loggedIn: false
}

interface LOGIN { type: 'LOGIN', payload: { user: UserProfile } }
interface LOGOUT { type: 'LOGOUT' }
type Actions = LOGIN | LOGOUT

const loginAction = (user: UserProfile): LOGIN => ({ type: 'LOGIN', payload: { user } })
const logoutAction = (): LOGOUT => ({ type: 'LOGOUT' })

export const UserContextProvider: FC = ({ children }) => {

  const [state, dispatch] = useReducer((state: State, action: Actions) => {
    switch (action.type) {
      case 'LOGIN': return { ...state, user: action.payload.user, loggedIn: true }
      case 'LOGOUT': return { ...state, user: null, loggedIn: false }
      default: return state;
    }
  }, initialState)

  const login = async () => {
    const user = await userService.getUserProfile()
    dispatch(loginAction(user))
  }
  const logout = () => {
    dispatch(logoutAction())
  }

  return (
    <UserContext.Provider value={{ ...state, login, logout }}>
      {children}
    </UserContext.Provider>
  )
}
