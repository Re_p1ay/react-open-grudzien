import axios, { AxiosError, CancelTokenSource } from "axios";
import { Album } from "../model/Album";
import { AlbumsSearchResponse } from "../model/Search";

export class AlbumSearchService {

  constructor() { }

  async getAlbumById(album_id: string, cancel?: CancelTokenSource) {
    const res = await axios.get<Album>('albums/' + album_id, {
      cancelToken: cancel?.token
    })
    return res.data
  }


  async searchAlbums(query: string/* ,cancel =  axios.CancelToken.source()*/) {

    // const cancelToken = new axios.CancelToken(cancel => { })
    const cancelToken = axios.CancelToken.source()


    const res = axios.get<AlbumsSearchResponse>('search', {
      params: {
        type: 'album',
        q: query
      },
      cancelToken: cancelToken.token
    });

    // cancelToken.cancel('Because.. Reasons...')

    return (await res).data.albums.items;
  }

  searchAlbumsCancellable(query: string) {

    // const cancelToken = new axios.CancelToken(cancel => { })
    const cancelToken = axios.CancelToken.source()


    const res = axios.get<AlbumsSearchResponse>('search', {
      params: {
        type: 'album',
        q: query
      },
      cancelToken: cancelToken.token
    });

    // cancelToken.cancel('Because.. Reasons...')

    const promise = res.then(res => res.data.albums.items) as CancellablePromise<Album[]>;

    (promise).token = cancelToken

    return promise
  }


  // searchAlbums(query: string) {
  //   return new Promise<Album[]>((resolve) => {
  //     setTimeout(() => {
  //       resolve(albumsMock as Album[])
  //     }, 1000)
  //   })
  // }
}

type CancellablePromise<T> = Promise<T> & { token: CancelTokenSource }