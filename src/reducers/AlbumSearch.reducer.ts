import { Reducer } from 'react';
import { Album } from '../core/model/Album';

type State = {
  error: string;
  loading: boolean;
  query: string;
  results: Album[];
};
export const initialState: State = { error: '', loading: false, query: '', results: [] };
interface SEARCH_START { type: 'SEARCH_START'; payload: { query: string; }; }
interface SEARCH_SUCCESS { type: 'SEARCH_SUCCESS'; payload: { results: Album[]; }; }
interface SEARCH_FAILED { type: 'SEARCH_FAILED'; payload: { error: any; }; }

type Actions = SEARCH_START |
  SEARCH_SUCCESS |
  SEARCH_FAILED;

export const reducer: Reducer<State, Actions> = (state, action) => {
  switch (action.type) {
    case 'SEARCH_START': return { ...state, error: '', loading: true, query: action.payload.query, results: [] };
    case 'SEARCH_SUCCESS': return { ...state, error: '', loading: false, results: action.payload.results };
    case 'SEARCH_FAILED': return { ...state, loading: false, error: action.payload.error?.message };
    default: return state;
  }

};
// Action Creators
export const searchStart = (query: string): SEARCH_START => ({ type: 'SEARCH_START', payload: { query } });
export const searchSuccess = (results: Album[]): SEARCH_SUCCESS => ({ type: 'SEARCH_SUCCESS', payload: { results } });
export const searchFailed = (error: any): SEARCH_FAILED => ({ type: 'SEARCH_FAILED', payload: { error } });
