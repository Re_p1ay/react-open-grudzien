import axios from "axios";
import { Action, Dispatch, Reducer } from "redux";
import { PaginigObject } from "../core/model/PaginigObject";
import { Playlist } from "../core/model/Playlist";
import { AppState } from "../store";

import { createSelector } from 'reselect'

export interface State {
  items: Playlist[]
  selectedId?: Playlist['id'],
  loading: boolean
  query: string
  error: string
}
export const featureKey = 'playlists'

const initialState: State = {
  items: [],
  loading: false,
  error: '',
  query: '',
}

interface LOAD_PLAYLISTS_START { type: 'LOAD_PLAYLISTS_START' }
interface LOAD_PLAYLISTS_SUCCESS { type: 'LOAD_PLAYLISTS_SUCCESS', payload: { data: Playlist[] } }
interface LOAD_PLAYLISTS_FAILED { type: 'LOAD_PLAYLISTS_FAILED', payload: { error: any } }
interface SELECT_PLAYLIST { type: 'SELECT_PLAYLIST', payload: { id: Playlist['id'] | undefined } }
type Actions =
  | LOAD_PLAYLISTS_START
  | LOAD_PLAYLISTS_SUCCESS
  | LOAD_PLAYLISTS_FAILED
  | SELECT_PLAYLIST


export const reducer: Reducer<State, Actions> = (state = initialState, action: Actions) => {
  switch (action.type) {
    case 'LOAD_PLAYLISTS_START': return { ...state, loading: true }
    case 'LOAD_PLAYLISTS_SUCCESS': return { ...state, loading: false, items: action.payload.data }
    case 'LOAD_PLAYLISTS_FAILED': return { ...state, loading: false, error: action.payload.error?.message }
    case 'SELECT_PLAYLIST': return { ...state, selectedId: action.payload.id }
    default: return state
  }
}

/* Action Creators */
const load_playlists_start = (): LOAD_PLAYLISTS_START => ({
  type: 'LOAD_PLAYLISTS_START'
})
const load_playlists_success = (data: Playlist[]): LOAD_PLAYLISTS_SUCCESS => ({
  type: 'LOAD_PLAYLISTS_SUCCESS', payload: { data }
})
const load_playlists_failed = (error: any): LOAD_PLAYLISTS_FAILED => ({
  type: 'LOAD_PLAYLISTS_FAILED', payload: { error }
})
export const select_playlist = (id: Playlist['id'] | undefined): SELECT_PLAYLIST => ({
  type: 'SELECT_PLAYLIST', payload: { id }
})

/* Async Action creators */
export const loadMyPlaylists = () => async (dispatch: Dispatch) => {
  try {
    dispatch(load_playlists_start())
    const { data: playlists } = await axios.get<PaginigObject<Playlist>>('me/playlists')
    dispatch(load_playlists_success(playlists.items))
  } catch (error) {
    dispatch(load_playlists_failed(error))
  }
}

export const savePlaylist = (draft: Playlist) => async (dispatch: Dispatch) => {
  try {
  } catch (error) { }
}

/* Selectors */

export const selectPlaylistsFeature = (state: AppState) => state[featureKey];

export const selectPlaylists = createSelector(
  selectPlaylistsFeature,
  (state: State) => (state).items
)

export const selectSelectedPlaylist = createSelector(
  selectPlaylists,
  selectPlaylistsFeature,
  (items, state) => items.find(p => p.id === state.selectedId))


