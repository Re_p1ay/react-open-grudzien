
import { Dispatch, Reducer } from 'redux';
import { Album } from '../core/model/Album';
import { albumSearch } from '../core/services';
import { AppState } from '../store';

export type State = {
  error: string;
  loading: boolean;
  query: string;
  results: Album[];
};

export const featureKey = 'search'

export const initialState: State = { error: '', loading: false, query: '', results: [] };

interface SEARCH_START { type: 'SEARCH_START'; payload: { query: string; }; }
interface SEARCH_SUCCESS { type: 'SEARCH_SUCCESS'; payload: { results: Album[]; }; }
interface SEARCH_FAILED { type: 'SEARCH_FAILED'; payload: { error: any; }; }

type Actions = SEARCH_START |
  SEARCH_SUCCESS |
  SEARCH_FAILED;


export const reducer: Reducer<State, Actions> = (state = initialState, action) => {
  switch (action.type) {
    case 'SEARCH_START': return {
      ...state, error: '', loading: true, query: action.payload.query, results: []
    };
    case 'SEARCH_SUCCESS': return {
      ...state, error: '', loading: false, results: action.payload.results
    };
    case 'SEARCH_FAILED': return {
      ...state, loading: false, error: action.payload.error?.message
    };
    default: return state;
  }

};

// Action Creators
export const searchStart = (query: string): SEARCH_START => ({ type: 'SEARCH_START', payload: { query } });
export const searchSuccess = (results: Album[]): SEARCH_SUCCESS => ({ type: 'SEARCH_SUCCESS', payload: { results } });
export const searchFailed = (error: any): SEARCH_FAILED => ({ type: 'SEARCH_FAILED', payload: { error } });


// Async Action Createor

export const searchAlbums = (q: string | null) => (dispatch: Dispatch) => {
  if (!q) { return }
  // dispatch({ type: 'SEARCH_ALBUMS_ASYNC_ACTION' })

  (async () => {
    try {
      dispatch(searchStart(q))
      const results = await albumSearch.searchAlbums(q)
      dispatch(searchSuccess(results))
    } catch (error) {
      dispatch(searchFailed(error))
    }
  })()

  // dispatch(searchStart(q))
  // albumSearch.searchAlbums(q).then(results => {
  //   dispatch(searchSuccess(results))
  // }).catch(error => {
  //   dispatch(searchFailed(error))
  // })
}


/* Selectors */
export const selectFeature = (state: AppState) => state[featureKey]
export const selectResults = (state: AppState) => selectFeature(state).results
