
// tsrafc
import React, { useEffect, useReducer, ReducerAction } from 'react'
import { RouteComponentProps, useHistory, useLocation, useParams, useRouteMatch } from 'react-router-dom'
import { useSearchRequest } from '../../core/hooks/useSearchRequest'
import { albumSearch as albumSearchService } from '../../core/services'
import { SearchForm } from '../components/SearchForm'
import { SearchResults } from '../components/SearchResults'
import * as fromReducer from '../../reducers/AlbumSearch.reducer'

// =============

type Props = {} & RouteComponentProps<{ /* placki: string */ }>

export const AlbumSearchReduce = (props: Props) => {

  const [{
    results, query, loading, error
  }, dispatch] = useReducer(fromReducer.reducer, fromReducer.initialState)

  const location = useLocation()
  useEffect(() => {
    (async () => {
      try {
        const q = new URLSearchParams(location.search).get('q') || 'batman'
        dispatch(fromReducer.searchStart(q))

        const results = await albumSearchService.searchAlbums(q)
        dispatch(fromReducer.searchSuccess(results))

      } catch (error) {
        dispatch(fromReducer.searchFailed(error))
      }
    })()
  }, [location.search])

  const history = useHistory()
  const search = (query: string) => {
    history.replace('/search?q=' + query)
  }

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm onSearch={search} query={query || ''} />
        </div>
      </div>

      <div className="row">
        <div className="col">
          {loading && <p className="alert alert-info">Loading...</p>}
          {error && <p className="alert alert-danger">{error}</p>}
          {query && <p onClick={() => search('')}>Results for "{query}"</p>}
          {results && <SearchResults results={results} />}
        </div>
      </div>
    </div>
  )
}
