
// tsrafc
import React, { useEffect, useReducer, ReducerAction } from 'react'
import { RouteComponentProps, useHistory, useLocation, useParams, useRouteMatch } from 'react-router-dom'
import { useSearchRequest } from '../../core/hooks/useSearchRequest'
import { albumSearch as albumSearchService } from '../../core/services'
import { SearchForm } from '../components/SearchForm'
import { SearchResults } from '../components/SearchResults'
import * as fromReducer from '../../reducers/Search.reducer'
import { useDispatch, useSelector, useStore } from 'react-redux'
import { AppState } from '../../store'

// =============

type Props = {} & RouteComponentProps<{ /* placki: string */ }>

export const AlbumSearchRedux = (props: Props) => {
  // const { dispatch, getState } = useStore()
  // const { query, error, results, loading } = useSelector<AppState, fromReducer.State>(state => state.search)
  const { query, error, loading } = useSelector(fromReducer.selectFeature)
  const results = useSelector(fromReducer.selectResults)

  const dispatch = useDispatch()

  const location = useLocation()
  useEffect(() => {
    const query = new URLSearchParams(location.search).get('q') // || 'batman'
    dispatch(fromReducer.searchAlbums(query))
    // fromReducer.searchAlbums(query, dispatch)

  }, [location.search])

  const history = useHistory()
  const search = (query: string) => {
    history.replace('/search?q=' + query)
  }

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm onSearch={search} query={query || ''} />
        </div>
      </div>

      <div className="row">
        <div className="col">
          {loading && <p className="alert alert-info">Loading...</p>}
          {error && <p className="alert alert-danger">{error}</p>}
          {query && <p onClick={() => search('')}>Results for "{query}"</p>}
          {results && <SearchResults results={results} />}
        </div>
      </div>
    </div>
  )
}
