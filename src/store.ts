import { AnyAction, applyMiddleware, combineReducers, compose, createStore, Dispatch, Middleware, MiddlewareAPI, Reducer } from "redux";
import * as searchSlice from "./reducers/Search.reducer";
import * as counterSlice from "./reducers/Counter.reducer";
import * as playlistsSlice from "./reducers/Playlists.reducer";

import reduxThunk from 'redux-thunk'

export type AppState = {
  [searchSlice.featureKey]: searchSlice.State
  [playlistsSlice.featureKey]: playlistsSlice.State
}


const rootReducer = combineReducers({
  [counterSlice.featureKey]: counterSlice.reducer,
  [searchSlice.featureKey]: searchSlice.reducer,
  [playlistsSlice.featureKey]: playlistsSlice.reducer
})

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(rootReducer, composeEnhancers(applyMiddleware(
  reduxThunk,
)))

store.dispatch(counterSlice.incCounter(10))